package com.example.filestorage;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Button mBtnCreateFile, mBtnDeleteFile, mBtnFileList, mBtnReadFile;
    EditText mFileContent;
    TextView mOutputText;
    public static final String File_Name = "mytextfile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initviews();
//        step 4
        String path = getFilesDir().getAbsolutePath();
//          this path the path of internal storage

        mOutputText.setText(path);

//        Step 2
        this.mBtnCreateFile.setOnClickListener(view -> {
            try {
                createFile(view);
            } catch (IOException e) {
                    e.printStackTrace();
            }
        });
        this.mBtnFileList.setOnClickListener(this::showFileList);
        this.mBtnDeleteFile.setOnClickListener(this::deleteFile);
        this.mBtnReadFile.setOnClickListener(this::readFile);

    }

    private void createFile(View view) throws IOException {
//            step 3
        String data = mFileContent.getText().toString();

        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(File_Name, MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.flush();
//            if somehow file kisi kaaran se reh jaati h buffer m or physical device me store nhi hoti h
//            to ye use store krwa deta hai


            mOutputText.setText("file is written ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (outputStream != null) {
                outputStream.close();
            }

        }
    }

    private void deleteFile(View view) {


    }

    private void readFile(View view) {


    }

    private void showFileList(View view) {


    }

    //          Step 1
    private void initviews() {
        mBtnCreateFile = findViewById(R.id.createfile);
        mBtnDeleteFile = findViewById(R.id.deletefile);
        mBtnReadFile = findViewById(R.id.readfile);
        mBtnFileList = findViewById(R.id.showlist);
        mFileContent = findViewById(R.id.filecontent);
        mOutputText = findViewById(R.id.showtext);

    }
}
